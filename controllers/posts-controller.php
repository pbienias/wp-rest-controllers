<?php

if ( !defined( 'ABSPATH' ) ) exit;

/**
* Base REST Controller for Posts
*/
class BB_REST_Posts_Controller {

  /**
  * Endpoint params used in WP_Query for collection of posts
  */
  public $wp_query_params = array( 'per_page', 'page', 'offset' );

  /**
  * Array of taxonomy names of given post type
  */
  public $post_taxonomies;

  /**
  * Constructor
  *
  * @param String $post_type Post type
  * @param String $namespace Namespace of given post type endpoints
  * @param String $resource_name Endpoints name to be combined with namespace
  */
  function __construct( $post_type, $namespace, $resource_name ) {
    $this->post_type = $post_type;
    $this->namespace = $namespace;
    $this->resource_name = $resource_name;

    $this->post_taxonomies = get_object_taxonomies( $this->post_type );
  }

  /*
  Register REST endpoints basing on namespace and resource_name
  */
  public function register_routes() {
    register_rest_route( $this->namespace, '/' . $this->resource_name, array(
      array(
        'methods'             => WP_REST_Server::READABLE,
        'callback'            => array( $this, 'get_items' ),
        'permission_callback' => array( $this, 'get_items_permission_callback' ),
        'args'                => $this->get_collection_params()
      ),
      'schema'  => array( $this, 'get_item_schema' )
    ) );

    register_rest_route( $this->namespace, '/' . $this->resource_name . '/(?P<id>[\d]+)', array(
      array(
        'methods'             => WP_REST_Server::READABLE,
        'callback'            => array( $this, 'get_item' ),
        'permission_callback' => array( $this, 'get_items_permission_callback' ),
        'args'                => $this->get_item_params()
      ),
      'schema' => array( $this, 'get_item_schema' )
    ) );
  }

  /**
  * Retrieve single post with given id
  *
  * @param WP_REST_Request $request WP Request object
  * @return WP_REST_Response|WP_Error Response object if success, otherwise error
  */
  public function get_item( $request ) {
    $id = (int) $request['id'];
    $post = get_post( $id );

    if ( empty( $post ) ) {
      return new WP_Error( 'no_post', 'Poszukiwany wpis nie istnieje', array( 'status' => 404 ) );
    }

    $response = $this->prepare_item_for_response( $post, $request );

    return $response;
  }

  /**
  * Return list of posts
  *
  * @param WP_REST_Request $request WP Request object
  * @return WP_REST_Response|WP_Error Response object if success, otherwise error
  */
  public function get_items( $request ) {
    $wp_query_args = $this->prepare_query_args( $request );

    $posts = get_posts( $wp_query_args );

    $data = array();
    if ( empty( $posts ) ) {
      return rest_ensure_response( $data );
    }

    foreach( $posts as $post ) {
      $response = $this->prepare_item_for_response( $post, $request );
      $data[] = $this->prepare_response_for_collection( $response );
    }

    return rest_ensure_response( $data );
  }

  /**
  * Prepare single post data basing on controller's schema
  *
  * @param WP_Post $post WP Post object
  * @param WP_REST_Request $request WP Request object
  * @return Array Post data array
  */
  function prepare_item_data_for_response( $post, $request ) {
    $post_data = array();

    $schema = $this->get_item_schema();
    if ( isset( $schema['properties']['id'] ) ) {
      $post_data['id'] = (int) $post->ID;
    }

    if ( isset( $schema['properties']['post_title'] ) ) {
      $post_data['post_title'] = $post->post_title;
    }

    if ( isset( $schema['properties']['post_content'] ) ) {
      $post_data['post_content'] = apply_filters( 'the_content', $post->post_content, $post );
    }

    return $post_data;
  }

  /**
  * Prepare single post data object for REST response
  *
  * @param WP_Post $post Current Post object
  * @param WP_REST_Request $request WP Request object
  * @return WP_REST_Response WP Response object with data
  */
  public function prepare_item_for_response( $post, $request ) {
    $post_data = $this->prepare_item_data_for_response( $post, $request );

    return rest_ensure_response( $post_data );
  }

  /**
  * Prepares WP Response for returning collection
  *
  * @param WP_REST_Response $response WP Response object
  */
  public function prepare_response_for_collection( $response ) {
    if ( ! ( $response instanceof WP_REST_Response ) ) {
      return $resonse;
    }

    $data = (array) $response->get_data();

    return $data;
  }

  /**
  * Define permission for given post type
  *
  * @param WP_REST_Request $request WP Request object
  * @return Boolean True if permission is granted, otherwise false
  */
  public function get_items_permission_callback( $request ) {
    return true;
  }

  /**
  * Defines schema for single item (post)
  *
  * @return Array Post schema definition
  */
  public function get_item_schema() {

    $schema = array(
      'title'       => 'Single Post object',
      'post_type'   => $this->post_type,
      'type'        => 'object',
      'properties'  => array(
        'id'          => array(
          'type'      => 'integer',
          'readonly'  => true
        ),
        'post_title'  => array(
          'type'      => 'string',
          'readonly'  => true
        ),
        'post_content'  => array(
          'type'        => 'string',
          'readonly'    => true
        )
      )
    );

    return $schema;

  }

  /**
  * Defines possible endpoint arguments concerning post's taxonomies
  *
  * @param Array $args Array of endpoint parameters
  * @return Array Endpoint parameters extended by taxonomy arguments
  */
  private function get_taxonomy_args( $args ) {

    if ( !isset( $args ) ) {
      $args = array();
    }

    foreach( $this->post_taxonomies as $post_taxonomy ) {
      $args[$post_taxonomy] = array(
        'type'  => 'array',
        'items' => array(
          'type'  => 'integer'
        ),
        'default' => array()
      );
    }

    return $args;

  }

  /**
  * Static method to return accepted params for returning collection
  *
  * @return Array Array of accepted parameters
  */
  public static function _get_collection_params() {
    $args = array(
      'per_page' => array(
        'type'    => 'integer',
        'default' => 10
      ),
      'page' => array(
        'type'    => 'integer',
        'default' => 1
      ),
      'order' => array(
        'type'    => 'string',
        'default' => 'DESC'
      ),
      'orderby' => array(
        'type'    => 'string',
        'default' => 'date'
      ),
      'include' => array(
        'type'        => 'array',
        'description' => 'Array of included elements eg. include[]=doctors. It can also be nested as include[doctors][]=services',
        'default'     => array(),
        'items'       => array(
          'type'      => 'string'
        )
      )
    );

    return $args;
  }

  /**
  * Specifies allowed parameters for posts collection endpoint
  *
  * @return Array Array of accepted parameters with their descriptions (objects)
  */
  private function get_collection_params() {
    $args = self::_get_collection_params();

    $args = $this->get_taxonomy_args( $args );

    return $args;
  }

  /**
  * Specifies allowed parameters for single post
  *
  * @return Array Array of accepted parameters with their descriptions
  */
  private function get_item_params() {
    return array(
      'id' => array(
        'type' => 'integer'
      ),
      'include' => array(
        'type'        => 'array',
        'description' => 'Array of included elements eg. include[]=doctors. It can also be nested as include[doctors][]=services',
        'default'     => array(),
        'items'       => array(
          'type'      => 'string'
        )
      )
    );
  }


  /**
  * Prepares args for WP_Query used when returning collection of posts
  *
  * @param WP_REST_Request $request WP Request object
  * @return Array Array with args and their values used in WP Query
  */
  public function prepare_query_args( $request ) {
    $params = $request->get_params();

    $wp_query_args = array(
      'post_type'       => $this->post_type,
      'posts_per_page'  => $params['per_page'],
      'page'            => $params['page'],
      'offset'          => $params['per_page'] * ( $params['page'] - 1),
      'order'           => $params['order'],
      'orderby'         => $params['orderby']
    );

    $tax_query = array();
    foreach( $params as $key => $value ) {
      if ( in_array( $key, $this->post_taxonomies ) && !empty( $value ) && isset( $value ) && $value != 0 ) {
        array_push(
          $tax_query,
          array(
            'taxonomy'  => $key,
            'field'     => 'id',
            'terms'     => $value
          )
        );
      }
    }

    if ( !empty( $tax_query ) )
      $wp_query_args['tax_query'] = $tax_query;

    return $wp_query_args;
  }

  /**
  * Checks if post with given post_id exists
  *
  * @param Integer $post_id WP Post ID value
  * @return Boolean True if post exists, otherwise false
  */
  public static function if_post_exists( $post_id ) {
    return ( get_post_status( $post_id ) !== FALSE );
  }

}
