<?php

require_once 'bb_rest_products_controller.php';
require_once 'bb_rest_product_types_controller.php';

class Test_API extends WP_UnitTestCase {

	private $bb_rest_posts_controller;
	private $bb_rest_products_controller;

	public function setUp() {
		parent::setUp();
		global $wp_rest_server;
		$this->server = $wp_rest_server = new \WP_REST_Server;

		$this->factory->product = new WP_UnitTest_Factory_For_Post( $this->factory );
		$this->factory->product->default_generation_definitions['post_type'] = 'product';

		$this->bb_rest_posts_controller = new BB_REST_Posts_Controller( 'post', 'api/v1', 'posts' );
		$this->bb_rest_posts_controller->register_routes();

		$this->bb_rest_products_controller = new BB_REST_Products_Controller();
		$this->bb_rest_products_controller->register_routes();

		$this->bb_rest_product_types_controller = new BB_REST_Product_Types_Controller();
		$this->bb_rest_product_types_controller->register_routes();

		do_action( 'rest_api_init' );
		$this->register_product_post_type();
		$this->register_product_type_taxonomy();
	}

	private function register_product_type_taxonomy() {
		$args = array(
			'label'									=> 'Product type',
			'public'								=> true,
			'show_in_rest'					=> true,
			'rest_base'							=> 'product-types',
			'rest_controller_class' => 'BB_REST_Product_Types_Controller'
		);

		register_taxonomy( 'product_type', 'product', $args );
	}

	private function register_product_post_type() {
		$args = array(
			'label'									=> 'Products',
			'public'								=> true,
			'show_in_rest'					=> true,
			'rest_base'							=> 'products',
			'rest_controller_class' => 'BB_REST_Products_Controller'
		);

		register_post_type( 'product', $args );
	}

	public function test_post_get_item() {
		$post = $this->factory->post->create_and_get(
			array(
				'post_author' => $this->editor_id,
				'post_title' 	=> 'My post title'
			)
		);
		$request = new WP_REST_Request( 'GET', '/api/v1/posts/' . $post->ID );

		$response = $this->server->dispatch( $request );
		$data = $response->get_data();

		// Assertions
		$this->assertArrayHasKey( 'id', $data );
		$this->assertArrayHasKey( 'post_title', $data );
		$this->assertArrayHasKey( 'post_content', $data );
		$this->assertEquals( 'My post title', $data['post_title'] );
	}

	public function test_post_get_items() {
		$this->factory->post->create( array( 'post_author' => $this->editor_id ) );
		$this->factory->post->create( array( 'post_author' => $this->author_id ) );

		$request = new WP_REST_Request( 'GET', '/api/v1/posts' );
		$response = $this->server->dispatch( $request );
		$data = $response->get_data();

		// Assertions
		$this->assertEquals( 2, count( $data ) );
		foreach( $data as $single_data ) {
			$this->assertArrayHasKey( 'id', $single_data );
			$this->assertArrayHasKey( 'post_title', $single_data );
			$this->assertArrayHasKey( 'post_content', $single_data );
		}

	}

	public function test_product_get_item() {
		$product = $this->factory->product->create_and_get( array( 'post_author' => $this->editor_id, 'post_title' => 'My product' ) );
		$request = new WP_REST_Request( 'GET', '/api/v1/products/' . $product->ID );

		$response = $this->server->dispatch( $request );
		$data = $response->get_data();

		// Assertions
		$this->assertArrayHasKey( 'id', $data );
		$this->assertArrayHasKey( 'name', $data );
		$this->assertEquals( 'My product', $data['name'] );
	}

	public function test_product_get_item_with_include() {
		$product = $this->factory->product->create_and_get( array( 'post_title' => 'My product' ) );
		add_post_meta( $product->ID, 'price', 100 );
		$request = new WP_REST_Request( 'GET', '/api/v1/products/' . $product->ID );
		$request->set_query_params( array( 'include' => array( 'price' ) ) );

		$response = $this->server->dispatch( $request );
		$data = $response->get_data();

		// Assertions
		$this->assertArrayHasKey( 'price', $data );
		$this->assertEquals( 100, $data['price'] );
	}

	public function test_product_get_item_with_include_type() {
		$product = $this->factory->product->create_and_get( array( 'post_title' => 'My product' ) );
		$term = wp_insert_term( 'Product type 1', 'product_type' );

		wp_set_post_terms( $product->ID, 'Product type 1', 'product_type', $append = false );
		add_post_meta( $product->ID, 'price', 150 );

		$request = new WP_REST_Request( 'GET', '/api/v1/products/' . $product->ID );
		$request->set_query_params( array( 'include' => array( 'price', 'product_type' ) ) );

		$response = $this->server->dispatch( $request );
		$data = $response->get_data();

		// Assertions
		$this->assertInternalType( 'array', $data['product_types'], 'product_types is Array');
		$this->assertEquals( 1, count( $data['product_types'] ) );
		foreach( $data['product_types'] as $product_type ) {
			$this->assertArrayHasKey( 'id', $product_type );
			$this->assertArrayHasKey( 'name', $product_type );
		}
		$this->assertEquals( 'Product type 1', $data['product_types'][0]['name'] );
	}

	public function test_product_get_items() {
		$this->factory->product->create( array( 'post_title' => 'Product 1' ) );
		$this->factory->product->create( array( 'post_title' => 'Product 2' ) );
		$request = new WP_REST_Request( 'GET', '/api/v1/products' );

		$response = $this->server->dispatch( $request );
		$data = $response->get_data();

		// Assertions
		foreach( $data as $product ) {
			$this->assertArrayHasKey( 'id', $product );
			$this->assertArrayHasKey( 'name', $product );
		}
		$this->assertEquals( 'Product 2', $data[0]['name'] );
		$this->assertEquals( 'Product 1', $data[1]['name'] );
	}

	public function test_product_get_items_with_include() {
		$product_1 = $this->factory->product->create_and_get( array( 'post_title' => 'Product 1' ) );
		$product_2 = $this->factory->product->create_and_get( array( 'post_title' => 'Product 2' ) );
		add_post_meta( $product_1->ID, 'price', 100 );
		add_post_meta( $product_2->ID, 'price', 200 );
		$request = new WP_REST_Request( 'GET', '/api/v1/products' );
		$request->set_query_params( array( 'include' => array( 'price' ) ) );

		$response = $this->server->dispatch( $request );
		$data = $response->get_data();

		// Assertions
		foreach( $data as $product ) {
			$this->assertArrayHasKey( 'price', $product );
		}
		$this->assertEquals( 200, $data[0]['price'] );
		$this->assertEquals( 100, $data[1]['price'] );
	}

}
