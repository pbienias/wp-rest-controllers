<?php

class BB_REST_Products_Controller extends BB_REST_Posts_Controller {

	public function __construct() {
		parent::__construct( 'product', 'api/v1', 'products' );
	}

	public function prepare_item_data_for_response( $product, $request ) {
		$product_data = array(
			'id'			=> (int) $product->ID,
			'name'		=> $product->post_title
		);

		$include = $request->get_param( 'include' );
		if ( is_array( $include ) && in_array( 'price', $include ) ) {
			$product_data['price'] = get_post_meta( $product->ID, 'price', true );
		}

		if ( is_array( $include ) && in_array( 'product_type', $include ) ) {
			$product_types = wp_get_post_terms( $product->ID, 'product_type' );
			$product_data['product_types'] = array();
			
			foreach( $product_types as $product_type ) {
				$product_data['product_types'][] = self::map_product_type( $product_type );
			}
		}

		return $product_data;
	}

	public static function map_product_type( $product_type ) {
		$data = array(
			'id' => $product_type->term_id,
			'name' => $product_type->name
		);

		return $data;
	}

	public function get_item_schema() {
		$schema = array(
      'title'       => 'Single Product object',
      'post_type'   => $this->post_type,
      'type'        => 'object',
      'properties'  => array(
        'id'          => array(
          'type'      => 'integer',
          'readonly'  => true
        ),
        'name'  => array(
          'type'      => 'string',
          'readonly'  => true
        ),
        'price'  => array(
          'type'        => 'string',
          'readonly'    => true
        )
      )
    );

		return $schema;
	}

}
