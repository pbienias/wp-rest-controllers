<?php


class BB_REST_Product_Types_Controller extends BB_REST_Terms_Controller {

  public function __construct() {
    parent::__construct( 'product_type', 'api/v1', 'product-types' );
  }

}
